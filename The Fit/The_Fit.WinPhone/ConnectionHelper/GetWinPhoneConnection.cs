﻿using SQLite.Net;
using SQLite.Net.Platform.WinRT;
using The_Fit.Helper;
using The_Fit.WinPhone.ConnectionHelper;
using Windows.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(GetWinPhoneConnection))]
namespace The_Fit.WinPhone.ConnectionHelper
{
    public class GetWinPhoneConnection : ISQLiteConnection
    {
        public SQLiteConnection GetConnection()
        {
            var path = System.IO.Path.Combine(ApplicationData.Current.LocalFolder.Path,
                The_Fit.App.DbName);
            var platform = new SQLitePlatformWinRT();
            var connection = new SQLiteConnection(platform, path);
            return connection;
        }
    }
}
