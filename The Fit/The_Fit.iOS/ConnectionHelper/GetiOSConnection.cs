﻿using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;
using The_Fit.Helper;
using The_Fit.iOS.ConnectionHelper;

[assembly: Xamarin.Forms.Dependency(typeof(GetiOSConnection))]
namespace The_Fit.iOS.ConnectionHelper
{
    public class GetiOSConnection : ISQLiteConnection
    {
        public SQLiteConnection GetConnection()
        {
            var documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(documentPath, App.DbName);
            var platform = new SQLitePlatformIOS();
            var connection = new SQLiteConnection(platform, path);
            return connection;



        }
    }
}
