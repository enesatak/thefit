﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using The_Fit.MasterSayfalar;
using Xamarin.Forms;

namespace The_Fit.Views
{
    public class MyMasterPage : MasterDetailPage
    {
        public MyMasterPage()
        {
            
            Master = new MyMenu();
            Detail = new MyTabbedPage();
                
        }
    }
}
