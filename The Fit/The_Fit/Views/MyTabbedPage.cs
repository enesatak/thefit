﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using The_Fit.TabbedSayfalar;
using Xamarin.Forms;

namespace The_Fit.Views
{
    public class MyTabbedPage : TabbedPage
    {
        public MyTabbedPage()
        {
            
            var navigationPage = new NavigationPage(new MyTab1());
            navigationPage.Icon = "home.png";
            navigationPage.Title = "Anasayfa";
            navigationPage.BarBackgroundColor = Color.FromRgb(21, 32, 42);
            navigationPage.BarTextColor = Color.Aqua;
            

            var navigationPage2 = new NavigationPage(new MyTab2());
            navigationPage2.Icon = "group.png";
            navigationPage2.Title = "Notlarım";
            navigationPage2.BarBackgroundColor = Color.FromRgb(21, 32, 42);
            navigationPage2.BarTextColor = Color.Aqua;

            var navigationPage3 = new NavigationPage(new MyTab3());
            navigationPage3.Icon = "water.png";
            navigationPage3.Title = "Su";
            navigationPage3.BarBackgroundColor = Color.FromRgb(21, 32, 42);
            navigationPage3.BarTextColor = Color.Aqua;

            Children.Add(navigationPage);
            Children.Add(navigationPage2);
            Children.Add(navigationPage3);
        }
    }
}
