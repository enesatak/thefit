﻿using System;
using The_Fit.Helper;
using The_Fit.Models;
using The_Fit.Views;
using Xamarin.Forms;

namespace The_Fit.DetailSayfalar
{
    public partial class ActivityPage : ContentPage
    {
        public ActivityPage()
        {
            InitializeComponent();
           
        }

        private void onInsert(object sender, EventArgs e)
        {
            SQLiteManager manager = new SQLiteManager();
            Activity _activity = new Models.Activity();
            _activity.Tarih = txtTarih.Text;
            _activity.Il = txtIl.Text;
            _activity.Ilce = txtIlce.Text;
            _activity.Yer = txtYer.Text;
            _activity.Saat = txtSaat.Text;
            _activity.Aktivite = txtAktivite.Text;

            int  isInserted = manager.Insert(_activity);
            if(isInserted > 0)
            {
                DisplayAlert("Başarılı", _activity.Aktivite + " eklendi.", "Ok");
                Navigation.PushModalAsync(new MyMasterPage());
            }
            else
            {
                DisplayAlert("Başarısız", _activity.Aktivite + " eklenemedi.", "Ok");
            }
            
        }
    }
}
