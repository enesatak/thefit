﻿using System;
using The_Fit.Models;
using The_Fit.Views;
using Xamarin.Forms;

namespace The_Fit.DetailSayfalar
{
    public partial class ActivityDetailPage : ContentPage
    {
        
        public ActivityDetailPage(Activity _activity)
        {
            InitializeComponent();

            lblIl.Text = _activity.Il;
            lblIlce.Text = _activity.Ilce;
            lblYer.Text = _activity.Yer;
            lblTarih.Text = _activity.Tarih;
            lblSaat.Text = _activity.Saat;
            lblActivity.Text = _activity.Aktivite;

        }

        private void onClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new MyMasterPage());
        }
    }
}
