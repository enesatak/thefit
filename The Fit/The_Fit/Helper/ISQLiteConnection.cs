﻿using SQLite.Net;

namespace The_Fit.Helper
{
    public interface ISQLiteConnection
    {
        //Burada her ortamın ayrı ayrı SQLite Connetion bilgilerini alıyoruz
        SQLiteConnection GetConnection();
    }
}
