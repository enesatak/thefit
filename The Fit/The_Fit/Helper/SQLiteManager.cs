﻿using SQLite.Net;
using System.Collections.Generic;
using The_Fit.Models;
using Xamarin.Forms;

namespace The_Fit.Helper
{
    public class SQLiteManager
    {
        SQLiteConnection _sqlconnection;

        public SQLiteManager()
        {
            _sqlconnection = DependencyService.Get<ISQLiteConnection>().GetConnection();
            _sqlconnection.CreateTable<Activity>();
            _sqlconnection.CreateTable<Members>();
        }

        #region CRUD
        public int Insert(Activity a)
        {
            return _sqlconnection.Insert(a);
        }

        public int Update(Activity a)
        {
            return _sqlconnection.Update(a);
        }

        public int Delete(int Id)
        {
            return _sqlconnection.Delete<Activity>(Id);
        }

        public IEnumerable<Activity> GetAll()
        {
            return _sqlconnection.Table<Activity>();
        }

        public Activity Get(int Id)
        {
            return _sqlconnection.Table<Activity>().Where(x => x.Id == Id).FirstOrDefault();
        }

        public void Dispose()
        {
            _sqlconnection.Dispose();
        }
        #endregion

        #region CRUD2
        public int Insert(Members m)
        {
            return _sqlconnection.Insert(m);
        }

        public int Update(Members m)
        {
            return _sqlconnection.Update(m);
        }

        public int Delete2(int Id)
        {
            return _sqlconnection.Delete<Members>(Id);
        }

        public IEnumerable<Members> GetAll2()
        {
            return _sqlconnection.Table<Members>();
        }

        public Members Get2(int Id)
        {
            return _sqlconnection.Table<Members>().Where(x => x.Id == Id).FirstOrDefault();
        }

        public void Dispose2()
        {
            _sqlconnection.Dispose();
        }
        #endregion

    }
}
