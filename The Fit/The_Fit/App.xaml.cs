﻿using The_Fit.MemberSayfalar;
using Xamarin.Forms;

namespace The_Fit
{
    public partial class App : Application
    {
        public static string DbName { get; set; } = "TheFit.db3";
        public App()
        {
            InitializeComponent();
            MainPage = new LoginPage();
        }
    }
}
