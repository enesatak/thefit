﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace The_Fit.TabbedSayfalar
{
    
    public partial class MyTab3 : ContentPage
    {
                public int water = 0;
                public int clickWater = 0;
                public int clickSu = 0;

                public int ml = 0;
                public int militre = 0;
                public int militre2 = 0;
        public MyTab3()
        {
            InitializeComponent();

            ImageAnimation();
        }

        private void OnAdd(object sender, EventArgs e)
        {
                
                clickWater = water;
                clickWater += 1;
                lblWater.Text = String.Format("{0} Bardak",
                                           clickWater);
                water = clickWater;

                militre = ml;
                militre += 250;
                lblMl.Text = String.Format("{0} ml",
                                           militre);
                ml = militre;
            
        }

        
        private void OnExtraction(object sender, EventArgs e)
        {
            
            if (water>0)
            {
                clickSu = water;
                clickSu -= 1;
                lblWater.Text = String.Format("{0} Bardak",
                                           clickSu);
                water = clickSu;

                militre2 = ml;
                militre2 -= 250;
                lblMl.Text = String.Format("{0} ml",
                                           militre2);
                ml = militre2;

            }

            else if (water == 0)
            {
                DisplayAlert("Başarısız", "Çıkarmak için önce su eklemelisiniz.", "Tamam");
            }
        }

        private void ImageAnimation()
        {
            su.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async (t) =>
                {
                    int i = 0;
                    while(true)
                    {
                        if(i % 2 == 0)
                        {
                            await su.LayoutTo(new Rectangle(0, 10,
                                su.Width, su.Height), 1250, Easing.SpringIn);
                        }
                        else
                        {
                            await su.LayoutTo(new Rectangle(0, 20,
                               su.Width, su.Height), 1250, Easing.SpringIn);
                        }
                        i++;
                    }
                    //await su.ScaleTo(0.90, 1250, Easing.CubicInOut);
                    //await su.ScaleTo(1, 50, Easing.CubicIn);
                    //await su.LayoutTo(new Rectangle(0, 100, su.Width, su.Height), 1250, Easing.SpringIn);
                    //DisplayAlert("Title", "Message", "Ok", "Cancel");

                })
            });
        }
    }
}