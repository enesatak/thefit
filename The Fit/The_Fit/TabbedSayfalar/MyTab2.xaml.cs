﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using The_Fit.DetailSayfalar;
using The_Fit.Helper;
using The_Fit.Models;
using Xamarin.Forms;

namespace The_Fit.TabbedSayfalar
{
    public partial class MyTab2 : ContentPage
    {
        SQLiteManager manager;
        public MyTab2()
        {
            InitializeComponent();

            List<Activity> activityList = new List<Activity>();
            manager = new SQLiteManager();
            activityList = manager.GetAll().ToList();

            lstActivitys.BindingContext = activityList;
        }

        private void onAdd(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ActivityPage());
        }

        private void onMenuRefresh(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void onMenuDelete(object sender, EventArgs e)
        {
            var selectedMenuItem = (MenuItem)sender;
            int isDeleted = manager.Delete(Convert.ToInt32(selectedMenuItem.CommandParameter.ToString()));
            if(isDeleted>0)
            {
                DisplayAlert("Başarılı", "Silindi", "Ok");
                RefreshData();
            }
            else
            {
                DisplayAlert("Başarısız", "Silinemedi", "Ok");
            }

        }

        private void onMenuDetail(object sender, EventArgs e)
        {
            var selectedMenuItem = (MenuItem)sender;
            var selectedActivity = manager.Get(Convert.ToInt32(selectedMenuItem
                .CommandParameter.ToString()));
            Navigation.PushModalAsync(new ActivityDetailPage(selectedActivity));

        }

        private void RefreshData()
        {
            List<Activity> activityList = new List<Activity>();
            activityList = manager.GetAll().ToList();
            lstActivitys.BindingContext = activityList;
        }
    }
}
