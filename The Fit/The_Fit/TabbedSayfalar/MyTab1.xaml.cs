﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using The_Fit.DetailSayfalar;
using Xamarin.Forms;

namespace The_Fit.TabbedSayfalar
{
    public partial class MyTab1 : ContentPage
    {
        public MyTab1()
        {
            InitializeComponent();
        }

        private void onStep(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new StepPage());
        }
    }
}
