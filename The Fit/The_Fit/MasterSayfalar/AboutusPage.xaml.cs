﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using The_Fit.Views;
using Xamarin.Forms;

namespace The_Fit.MasterSayfalar
{
    public partial class AboutusPage : ContentPage
    {
        public AboutusPage()
        {
            InitializeComponent();
        }

        private void onClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new MyMasterPage());
        }
    }
}
