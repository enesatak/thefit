﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using The_Fit.Helper;
using The_Fit.MemberSayfalar;
using The_Fit.Models;
using Xamarin.Forms;

namespace The_Fit.MasterSayfalar
{
    public partial class MyMenu : ContentPage
    {
        
        public MyMenu()
        {
            InitializeComponent();
            Title = "Menu";
            Icon = "menu.png";
            
        }
        private void OnChanged(object sender, ToggledEventArgs e)
        {
            string isOk=(e.Value.ToString());
            if (isOk == "True")
            {
                //True
                Application.Current.Resources["DefaultBackground"] =
                    Application.Current.Resources["TrueBackground"];

                Application.Current.Resources["DefaultLabel"] =
                    Application.Current.Resources["TrueLabel"];
                

            }
            else
            {
                //False
                Application.Current.Resources["DefaultBackground"] =
                    Application.Current.Resources["FalseBackground"];

                Application.Current.Resources["DefaultLabel"] =
                    Application.Current.Resources["FalseLabel"];
                

            }
        }

        private void onQuit(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoginPage());
        }

        private void onAbout(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new AboutusPage());
        }
    }
}
