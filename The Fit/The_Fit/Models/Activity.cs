﻿using SQLite.Net.Attributes;

namespace The_Fit.Models
{
    public class Activity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Il { get; set; }
        public string Ilce { get; set; }
        public string Yer { get; set; }
        public string Tarih { get; set; }
        public string Saat { get; set; }
        public string Aktivite { get; set; }

    }
}
