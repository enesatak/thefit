﻿using System;
using The_Fit.Helper;
using The_Fit.Models;
using The_Fit.Views;
using Xamarin.Forms;

namespace The_Fit.MemberSayfalar
{
    public partial class SignupPage : ContentPage
    {
        public SignupPage()
        {
            InitializeComponent();

        }
        
        public void onInsert(object sender, EventArgs e)
        {
            SQLiteManager manager = new SQLiteManager();
            Members _members = new Models.Members();
            _members.Name = txtName.Text;
            _members.Email = txtEmail.Text;
            _members.Password = txtPassword.Text;
            int isInserted = manager.Insert(_members);
            if (isInserted > 0)
            {
                DisplayAlert("Hoşgeldin", _members.Name, "Tamam");
                Navigation.PushModalAsync(new MyMasterPage());
            }
            else
            {
                DisplayAlert("Başarısız", _members.Name + "Böyle bir kullanıcı mevcut.", "Ok");
            }
            
        }

        private void onClickedLoginPage(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoginPage());
        }
    }
}
