﻿using System;
using System.Collections.Generic;
using System.Linq;
using The_Fit.Helper;
using The_Fit.Models;
using The_Fit.Views;
using Xamarin.Forms;

namespace The_Fit.MemberSayfalar
{
    public partial class LoginPage : ContentPage
    {
        SQLiteManager manager;
        public LoginPage()
        {
            InitializeComponent();

        }

        private void onLogin(object sender, EventArgs e)
        {
            List<Members> membersList = new List<Members>();
            manager = new SQLiteManager();
            membersList = manager.GetAll2().ToList();
            
            
            

            if (loginemail.Text != " ")
            { 
                DisplayAlert("Giriş Başarılı", "Tekrar Hoşgeldin", "Tamam");
                Navigation.PushModalAsync(new MyMasterPage());
            }
            else if(membersList.Count==0){
                DisplayAlert("Hoşgeldiniz", "Yeni kayıt oluşturunuz.", "Ok");
            }
            else { 
            DisplayAlert("Başarısız", "E-posta veya şifre yanlış.", "Ok");
            }

        }

        private void onSignupPage(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SignupPage());
        }
    }
}
