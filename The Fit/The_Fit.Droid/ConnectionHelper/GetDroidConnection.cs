using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;
using The_Fit.Droid.ConnectionHelper;
using The_Fit.Helper;

[assembly: Xamarin.Forms.Dependency(typeof(GetDroidConnection))]
namespace The_Fit.Droid.ConnectionHelper
{
    public class GetDroidConnection : ISQLiteConnection
    {
        public SQLiteConnection GetConnection()
        {
            string documentPath = System.Environment.GetFolderPath(System.
                Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(documentPath, App.DbName);
            var platform = new SQLitePlatformAndroid();
            var connection = new SQLiteConnection(platform, path);
            return connection;
        }
    }
}